# from get_name_vector import load_name_bert_model as name_model


from keras.layers import *

import numpy as np
from keras.applications.inception_v3 import InceptionV3
from keras.layers import *
from keras.models import Model
from keras.preprocessing.image import load_img, img_to_array
from skimage import transform

import tensorflow as tf
from flask import Flask, jsonify
from flask import request
#import config
import os

try:
    temp = os.listdir(".")
    print(temp)
except:
    pass
try:
    temp = os.listdir("/app/filestore/")
    print(temp)
except:
    pass

# from filestore.k8.vector_service.ir_vector.resnet_old.model import deep_rank_model as resnet_model
# from filestore.k8.vector_service.ir_vector.inception_old.model import deep_rank_model as inception_model
# from filestore.k8.vector_service.ir_vector.vgg_old.model import deep_rank_model as vgg_model


from get_image_vector1 import deep_rank_model as resnet_model
from get_image_vector2 import deep_rank_model as inception_model
from get_image_vector3 import deep_rank_model as vgg_model
from get_name_vector import load_name_bert_model as name_model
from get_name_desc_vector import load_name_description_bert_model as name_desc_model

app = Flask(__name__)
model = None
resnet_model_loaded = None
inception_model_loaded =None
vgg_model_loaded = None
# global model
# model = keras.models.load_model(MODEL_FILE,
#               custom_objects={'AttentionWithContext': AttentionWithContext})
# global graph
# graph = tf.get_default_graph()







# global graph
# graph = tf.get_default_graph()
def load_model():
    resnet_model_file = "filestore/k8/vector_service/ir_vector/resnet_old/model.h5"
    inception_model_file = "filestore/k8/vector_service/ir_vector/inception_old/model.h5"
    vgg_model_file = "filestore/k8/vector_service/ir_vector/vgg_old/model.h5"

    global resnet_model_loaded
    global inception_model_loaded
    global vgg_model_loaded

    resnet_model_loaded = resnet_model()
    resnet_model_loaded.load_weights(resnet_model_file)
    inception_model_loaded = inception_model()
    inception_model_loaded.load_weights(inception_model_file)
    vgg_model_loaded = vgg_model()
    vgg_model_loaded.load_weights(vgg_model_file)

    global graph
    graph = tf.get_default_graph()








# resnet_model_file = "filestore/k8/vector_service/ir_vector/resnet_old/model.h5"
# global resnet_model_loaded
# resnet_model_loaded = resnet_model()
# resnet_model_loaded.load_weights(resnet_model_file)
# global resnet_graph
# resnet_graph = tf.get_default_graph()
# print('===============================')
# print('resnet weights loaded')
#
# inception_model_file = "filestore/k8/vector_service/ir_vector/inception_old/model.h5"
# global inception_model_loaded
# inception_model_loaded = inception_model()
# inception_model_loaded.load_weights(inception_model_file)
# global inception_graph
# inception_graph = tf.get_default_graph()
# print('===============================')
# print('inception weights loaded')
#
#
# vgg_model_file = "filestore/k8/vector_service/ir_vector/vgg_old/model.h5"
# global vgg_model_loaded
# vgg_model_loaded = vgg_model()
# vgg_model_loaded.load_weights(vgg_model_file)
# global vgg_graph
# vgg_graph = tf.get_default_graph()
# print('===============================')
# print('vgg weights loaded ')

# def find_image(id,prepath):
#     filepath = prepath+"/"+id+".jpg"
#     image = load_img(filepath)
#     image = img_to_array(image).astype("float64")
#     image = transform.resize(image, (224, 224))
#     image *= 1. / 255
#     image = np.expand_dims(image, axis = 0)
#     return image




@app.route('/_ah/health')
def default():
    return 'ok'


@app.route('/vectorize', methods=['POST'])
def vectorize_the_fuck():
    post = request.json
    # id = str(post["_id"]).replace("ObjectId(\"",'').replace("\")",'')
    id = str(post["_id"]).replace("ObjectId(\"", '').replace("\")", '')
    print(id)

    try:
        filepath = 'filestore/combined_products_2019_03_14' + "/" + id + ".jpg"
        image = load_img(filepath)
        image = img_to_array(image).astype("float64")
        image = transform.resize(image, (224, 224))
        image *= 1. / 255
        image = np.expand_dims(image, axis=0)

        with graph.as_default():
            resnet_embedding = resnet_model_loaded.predict([image])[0].tolist()
            inception_embedding = inception_model_loaded.predict([image])[0].tolist()
            vgg_embedding = vgg_model_loaded.predict([image])[0].tolist()


        #
        # with resnet_graph.as_default():
        #     resnet_embedding = resnet_model_loaded.predict([image])[0].tolist()
        #
        # with inception_graph.as_default():
        #     inception_embedding = inception_model_loaded.predict([image])[0].tolist()
        # with vgg_graph.as_default():
        #     vgg_embedding = vgg_model_loaded.predict([image])[0].tolist()



    except Exception as e:
        post['error'] = str(e)
        resnet_embedding = [0] * 1024
        inception_embedding = [0] * 1024
        vgg_embedding = [0] * 1024

    name_embedding = name_model(post)
    name_desc_embedding = name_desc_model(post)
    post['name_vector'] = name_embedding
    post['name_desc_vector'] = name_desc_embedding
    post['resnet_vector'] = resnet_embedding
    post['inception_vector'] = inception_embedding
    post['vgg_vector'] = vgg_embedding

    return jsonify(post)


if __name__ == "__main__":
    print("loading keras model")
    load_model()

    # app.run(host='0.0.0.0',port=8080,debug=config.DEBUG_MODE)
    app.run(host='0.0.0.0', port=5006, debug=False)
